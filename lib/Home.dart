import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:async';
import 'package:sim_info/sim_info.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home();

  @override
  _HomeState createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  String _allowsVOIP;
  String _carrierName;
  String _isoCountryCode;
  String _mobileCountryCode;
  String _mobileNetworkCode;


  @override
  void initState() {
    super.initState();
    getSimInfo();
  }

  Future<void> getSimInfo() async {
    String allowsVOIP = await SimInfo.getAllowsVOIP;
    String carrierName = await SimInfo.getCarrierName;
    String isoCountryCode = await SimInfo.getIsoCountryCode;
    String mobileCountryCode = await SimInfo.getMobileCountryCode;
    String mobileNetworkCode = await SimInfo.getMobileNetworkCode;

    setState(() {
      _allowsVOIP = allowsVOIP;
      _carrierName = carrierName;
      _isoCountryCode = isoCountryCode;
      _mobileCountryCode = mobileCountryCode;
      _mobileNetworkCode = mobileNetworkCode;
    });
  }

  Widget home(BuildContext context) {
    return new Material(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            child: const Text('Show Network Info'),
            color: Theme.of(context).accentColor,
            elevation: 6.0,
            splashColor: Colors.amberAccent,
            textColor: const Color(0xFFFFFFFF),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    _buildNetworkInfoDialog(context),
              );
            },
          ),
          RaisedButton(
            child: const Text('Go back to login'),
            color: Theme.of(context).accentColor,
            elevation: 4.0,
            splashColor: Colors.amberAccent,
            textColor: const Color(0xFFFFFFFF),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  Widget _buildNetworkInfoDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Network Info:'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('VOIP : $_allowsVOIP '),
          Text('CarrierName : $_carrierName'),
          Text('ISO CountryCode : $_isoCountryCode'),
          Text('Mobile CountryCode : $_mobileCountryCode'),
          Text('Mobile NetworkCode : $_mobileNetworkCode'),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Okay, got it!'),
        ),
      ],
    );
  }

  Widget _buildLogoAttribution() {
    return new Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: new Image.asset(
              "assets/flutter.png",
              width: 32.0,
            ),
          ),
          const Expanded(
            child: const Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: const Text(
                'Popup window is like a dialog box that gains complete focus when it appears on screen.',
                style: const TextStyle(fontSize: 12.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: const EdgeInsets.only(top: 100.0),
//      color: Theme.of(context).scaffoldBackgroundColor,
        decoration: new BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          image: new DecorationImage(
            image: new AssetImage("assets/image_03.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: new Center(
          child: Column(
            children: <Widget>[
              home(context),
            ],
          ),
        ));
  }
}
